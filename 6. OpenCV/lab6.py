# -*- coding: utf-8 -*-
"""
Created on Sat Dec 10 15:43:46 2022

@author: Max
"""

import cv2 as cv
from glob import glob
from os import path, mkdir

IMG_DIR = "New Year/*"
exp = 'png'

pth = IMG_DIR.replace('/*', ' (%s)' % exp)
if not path.exists(pth):
    mkdir(pth)
for s in glob(IMG_DIR):
    img = cv.imread(s)
    pth_img = path.join(pth, path.basename(s).replace(s[s.rfind('.')+1:], exp))
    cv.imwrite(pth_img, img)
