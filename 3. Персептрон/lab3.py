# -*- coding: utf-8 -*-
"""
Created on Sun Nov 20 17:33:48 2022

@author: Max
"""

# Распознавание дорожных знаков ограничения скорости 20, 30, 60

import numpy as np

W = np.zeros((3,50))

X = np.array([
    [0,1,1,1,0,0,0,1,1,0,
     1,0,0,0,1,0,1,0,0,1,
     0,0,1,1,0,0,1,0,0,1,
     0,1,0,0,0,0,1,0,0,1,
     1,1,1,1,1,0,0,1,1,0],
    [0,1,1,1,0,0,0,1,1,0,
     1,0,0,0,1,0,1,0,0,1,
     0,0,1,1,0,0,1,0,0,1,
     1,0,0,0,1,0,1,0,0,1,
     0,1,1,1,0,0,0,1,1,0],
    [0,0,1,1,1,0,0,1,1,0,
     0,1,0,0,0,0,1,0,0,1,
     1,1,1,1,0,0,1,0,0,1,
     1,0,0,0,1,0,1,0,0,1,
     0,1,1,1,0,0,0,1,1,0]
    ])

Y = np.array([[1,0,0],
              [0,1,0],
              [0,0,1]])

α = 0.2
β = -0.4
σ = lambda x: 1 if x > 0 else 0

def f(x, _w):
    s = β + np.sum(x @ _w)
    return σ(s)

def train(W, X, Y):
    _w = W.copy()
    for i in range(W.shape[0]):
        for x, y in zip(X, Y[i]):
            W[i] += α * (y - f(x, W[i])) * x
    return (W != _w).any()

while train(W, X, Y):
    np.set_printoptions(linewidth=30)
    print(W)

Xtest = np.array([
    [1,1,1,1,0,0,0,1,1,0,
     1,0,0,0,1,0,1,0,0,1,
     0,0,0,1,0,0,1,0,0,1,
     0,0,0,0,1,0,1,0,0,1,
     0,1,1,1,1,0,0,1,1,0],
    [0,0,1,1,0,0,0,1,1,0,
     0,1,0,0,0,0,1,0,0,1,
     1,1,1,1,0,0,1,0,0,1,
     1,0,0,1,0,0,1,0,0,1,
     0,1,1,1,0,0,0,1,1,0],
    [0,1,1,1,0,0,0,1,1,0,
     1,0,0,0,1,0,1,0,0,1,
     0,0,1,1,0,0,1,0,0,1,
     0,1,0,0,0,0,1,0,0,1,
     1,1,1,1,1,0,0,1,1,0]
    ])

rez = [20, 30, 60]

for x in Xtest:
    for i in range(W.shape[0]):
        if f(x, W[i]):
            print(rez[i])
