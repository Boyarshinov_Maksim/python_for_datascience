# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 16:33:34 2022

@author: Max
"""

from sklearn.neural_network import MLPClassifier

import numpy as np
import pandas as pd

sofas = pd.ExcelFile("price.xlsx").parse('Лист1')

X = sofas.values[1:, [4,5,6,9,13,14,16]].astype(np.double)
Y = sofas.values[1:, 17].astype(np.int32)

HP = sorted(set(Y))

for i in range(len(X[0])):
    a, b = np.polyfit(np.sort(X[:,i]),np.linspace(0,1,len(X[:,i])),1)
    X[:,i] = X[:,i] * a + b

Y0 = np.zeros((len(Y), len(HP)))
for i, y in enumerate(Y):
    Y0[i,HP.index(y)] = 1.0

clf = MLPClassifier(
    solver='lbfgs', 
    alpha=1e-5,
    hidden_layer_sizes=(14,14), 
    random_state=1,
    max_iter=100, 
    warm_start=True
)

for x in range(100):
    clf.fit(X, Y0)

Y_ = clf.predict(X) @ HP

print("\n  Y   -   Y_  = Y-Y_")
for a, b in zip(Y, Y_):
    print(a, "-", b, "=", a-b)

# Результат в зависимости от параметра hidden_layer_sizes меняется следующим образом:
# - при одном скрытом слое при увеличении числа его элементов, начиная с 1,
#   результат становится более точным, а начиная с 11 элементов результат абсолютно точный;
# - при двух скрытых слоях результат так же улучшается с увеличением элементов в каждом слое,
#   а начиная с 14 элементов в каждом слое результат абсолютно точный.
