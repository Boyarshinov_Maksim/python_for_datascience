# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 14:15:47 2022

@author: Max
"""
# Распознавание букв "А" и "Б"

import numpy as np
from scipy.ndimage import rotate

w0 = np.zeros((25))
w1 = np.zeros((25))

D = np.array([
    [0,0,1,0,0,
     0,1,0,1,0,
     0,1,0,1,0,
     1,1,1,1,1,
     1,0,0,0,1],
    [1,1,1,1,1,
     1,0,0,0,0,
     1,1,1,1,0,
     1,0,0,0,1,
     1,1,1,1,0],
    [0,0,1,0,0,
     0,1,0,1,0,
     0,1,1,1,0,
     0,1,0,1,0,
     1,0,0,0,1],
    [1,1,1,1,1,
     1,0,0,0,0,
     1,1,1,1,1,
     1,0,0,0,1,
     1,1,1,1,1],
    [0,0,0,1,0,
     0,0,1,1,0,
     0,1,0,0,1,
     0,1,1,1,1,
     1,0,0,0,1],
    [0,1,1,1,1,
     0,1,0,0,0,
     0,1,1,1,0,
     0,1,0,0,1,
     0,1,1,1,1]
    ])

Y0 = np.array([1,0,1,0,1,0])
Y1 = np.array([0,1,0,1,0,1])

α = 0.2
β = -0.4
σ = lambda x: 1 if x > 0 else 0

def f(x, _w):
    s = β + np.sum(x @ _w)
    return σ(s)

def train(w, D, Y):
    _w = w.copy()
    for x, y in zip(D, Y):
        w += α * (y - f(x, w)) * x
    return (w != _w).any()

while train(w0, D, Y0) and train(w1, D, Y1):
    np.set_printoptions(linewidth=30)
    print(w0)
    print(w1)

Dtest = np.array([
    [0,1,0,0,0,
     0,1,1,0,0,
     0,1,0,1,0,
     0,1,1,1,1,
     0,1,0,0,1],
    [0,0,1,1,0,
     0,1,0,0,0,
     1,1,1,1,1,
     1,0,0,0,1,
     0,1,1,1,1],
    [0,0,0,1,0,
     0,0,1,1,0,
     0,1,0,1,0,
     1,1,1,1,0,
     1,0,0,1,0],
    [1,1,1,1,1,
     0,1,0,0,0,
     0,1,1,1,0,
     0,1,0,0,1,
     1,1,1,1,1]
    ])

for x in Dtest:
    print(f(x, w0), f(x, w1))


#Оценка чувствительности модели к повороту изображения

def test(ф):
    M = np.zeros((2,2))
    for i in range(2):
        x = rotate(Dtest[i].reshape(5,5), ф, reshape=False).flatten()
        j = f(x, w1)
        M[i,j] += 1
    return M

rez = np.zeros((10,2,2))
for ф in range(100):
    rez[ф//10] += test(ф)/10
np.savetxt('out.csv', rez[1], delimiter=';', fmt='%.1f')
print(rez[0])
print(rez[1])
print(rez[4])
print(rez[7])
