# -*- coding: utf-8 -*-
"""
Created on Sat Dec  3 17:06:15 2022

@author: Max
"""

import numpy as np
import pandas as pd

# Загрузка и подготовка данных из Excel:

sofas = pd.ExcelFile("price.xlsx").parse('Лист1')

X = sofas.values[1:, [4,5,6,9,13,16,17]].astype(np.double)
Y = sofas.values[1:,14].astype(np.double)

rez = sorted(list(set(Y)))
W = np.zeros((len(rez), len(X[0])))
Y0 = np.zeros((len(rez), len(X)))
for i, y in enumerate(rez):
    Y0[i, np.where(Y==y)] = 1

# Обучение до шкалирования:
    
α = 0.2
β = -0.4
σ = lambda x: 1 if x > 0 else 0

def f(x, _w):
    s = β + np.sum(x @ _w)
    return σ(s)

def train(W, X, Y):
    _w = W.copy()
    for i in range(W.shape[0]):
        for x, y in zip(X, Y[i]):
            W[i] += α * (y - f(x, W[i])) * x
    return (W != _w).any()

n=0
while train(W, X, Y0) and n<20:
    n+=1

# Шкалирование:

X0 = np.array([[(X[i,j]-X[:,j].min()) / (X[:,j].max()-X[:,j].min())
        for j in range(len(X[0]))] for i in range(len(X))]).astype(np.double)

# Обучение после шкалирования:

W0=np.zeros_like(W)
n=0
while train(W0, X0, Y0) and n<21:
    n+=1

# Вывод результата:

REZ=np.zeros((len(X), 4))
for i in range(len(X)):
    REZ[i,0]=i
    REZ[i,1]=Y[i]
    for j in range(W.shape[0]):
        if f(X[i], W[j]):
            REZ[i,2]=rez[j]
    for j in range(W0.shape[0]):
        if f(X0[i], W0[j]):
            REZ[i,3]=rez[j]
print('  №|Ожидаем.|Без шк.|Шкалир.')
print(REZ)

# Результат после шкалирования значительно ближе к ожидаемому значению чем до шкалирования.
# Значение определяется почти всегда, но в некоторых случаях не совсем точно.
